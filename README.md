# ChatGPT Api

This project aim to provide a simple API to interact with OpenAI's ChatGPT Website. You need a ChatGPT account and a valid session running in your browser to use this API.

## Getting started

First, you need to install the extension in your browser. You can find the extension in the extension folder, and you can install it in your browser with the following button.

<img src="https://i.ibb.co/F81wVXN/image-2024-04-07-195244455.png">


After installing the extension, you can go to the ChatGPT website and start a session. You can find a hint to the status of the session in top right corner of the page.

Then, you can start the API server with the following command:

```bash
cd server
npm install
npm run start
```

The server will start in the port 3000. You can change the port in the `server/index.js` file.

You should see the "connected" status in the top right corner of the ChatGPT website. If you see the "disconnected" status, you should refresh the page.