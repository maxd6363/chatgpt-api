console.log("ChatListral helper loaded");

const connectedElement = document.createElement("h1");
document.body.appendChild(connectedElement);
connectedElement.innerText = "Disconnected";
connectedElement.style.position = "fixed";
connectedElement.style.top = "0";
connectedElement.style.right = "0";
connectedElement.style.zIndex = "9999";
connectedElement.style.margin = "10px";
connectedElement.style.marginRight = "120px";
connectedElement.style.color = "red";
connectedElement.style.fontSize = "35px";

let ws = null;
let connecting = false;

const sendToServer = (message) => {
  ws.send(message);
  console.log("Sent: '" + message + "'");
};

const cleanMessage = (message) => {
  return message.replace("ChatGPT", "");
};

function tryToConnect() {
  if (connecting) {
    console.log("Tentative de connexion en cours...");
    return;
  }

  console.log("Trying to connect to server");

  connecting = true;

  ws = new WebSocket("ws://localhost:3000");

  ws.onopen = function () {
    console.log("WebSocket Client Connected");
    connectedElement.innerText = "Connected";
    connectedElement.style.color = "green";
    connecting = false;

    if (sessionStorage.getItem("new") === "true") {
      console.log("Found new conversation flag, sending to server");
      sendToServer(
        JSON.stringify({
          action: "new_done",
        })
      );
      sessionStorage.removeItem("new");
    }
  };

  ws.onmessage = function (e) {
    const data = JSON.parse(e.data);
    console.log("Received: ", data);
    if (data.action === "message") {
      promptMessage(data.message);
      tryGetLatestMessage();
    }
    if (data.action === "new") {
      sessionStorage.setItem("new", "true");
      window.location.href = "/";
    }
  };

  ws.onerror = function (error) {
    console.error("WebSocket error: ", error);
    connecting = false;
    ws.close();
    setTimeout(tryToConnect, 50);
  };

  ws.onclose = function (e) {
    console.log("Connection closed");
    connectedElement.innerText = "Disconnected";
    connectedElement.style.color = "red";
    connecting = false;
    setTimeout(tryToConnect, 50);
  };
}

const promptMessage = (message) => {
  document.getElementsByTagName("textarea")[0].value = message;

  document.getElementsByTagName("textarea")[0].dispatchEvent(
    new Event("input", {
      bubbles: true,
      cancelable: true,
    })
  );

  document.getElementsByTagName("textarea")[0].nextSibling.click();
};

const tryGetLatestMessage = () => {
  let tries = 0;
  let secureTries = 0;
  // wait for the latest message to appear
  setTimeout(() => {
    const interval = setInterval(() => {
      tries++;
      if (tries > 25) {
        clearInterval(interval);
        console.log("Timeout");
        sendToServer(
          JSON.stringify({
            action: "error",
            message: "Timeout",
          })
        );
      }

      
      const messages = document.getElementsByClassName("prose");
      const latestMessage = messages[messages.length - 1];
      const prompt = document.getElementsByTagName("textarea")[0];
      const sibling = prompt.nextSibling;;

      console.log("Sibling: ", sibling);

      if (sibling.disabled) {
        const message = cleanMessage(latestMessage.innerText);
        console.log(`Got message with ${secureTries} secure tries : `, message);
        secureTries++;
        if (secureTries > 1) {
          clearInterval(interval);
          clearInterval(interval);
          sendToServer(
            JSON.stringify({
              action: "message",
              message: JSON.stringify(message),
            })
          );
        }
      }
    }, 1000);
  }, 500);
};

tryToConnect();
