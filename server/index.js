const express = require("express");
const cors = require("cors");

const app = express();
const expressWs = require("express-ws")(app);

let webSocket = null;

app.use(express.json());
app.use(
  cors({
    origin: "*",
  })
);

app.ws("/", function (ws, req) {
  console.log("Client connected");
  if (webSocket && webSocket._events.message) {
    console.log("Transferring events to new client");
    ws.on("message", webSocket._events.message);
  }
  webSocket = ws;
});

app.post("/chat", (req, res) => {
  if (webSocket) {
    webSocket.send(JSON.stringify({ action: "new" }));

    // we wait for the client to send us the response to return it to the user
    webSocket.on("message", function (msg) {
      msg = JSON.parse(msg);
      if (msg.action === "new_done") {
        res.json({ id: 0 });
      } else {
        res.status(500).send("Invalid action received from the extension");
      }
      webSocket.removeAllListeners("message");
    });
  } else {
    res.status(500).send("No client connected");
  }
});

app.post("/chat/:id", (req, res) => {
  if (webSocket) {
    webSocket.send(
      JSON.stringify({
        action: "message",
        message: req.body.message,
      })
    );

    // we wait for the client to send us the response to return it to the user
    webSocket.on("message", function (msg) {
      console.log("Received message from client : ", msg);
      msg = JSON.parse(msg);
      if (msg.action === "message") {
        console.log("Received message from extension:", msg.message);
        res.send(msg.message);
      } else {
        res.status(500).send("Invalid action received from the extension");
      }
      webSocket.removeAllListeners("message");
    });
  } else {
    res.status(500).send("No client connected");
  }
});

app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
